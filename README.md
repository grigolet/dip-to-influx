# DIP to InfluxDB
This repo contains a python script and docker image to insert messages from a 
list of DIP's subscriptions to an InfluxDB istance.

The DIP API's are the C++ ones, binded to python with `cppyy`.

The data is stored in a single measurement with two tags by default:
* A `sub` tag representing the name of the subscription
* A `dip_tag` representing the DIP message's tag (ex. `tag__DIP_DEFAULT__`). 
    Note that `tag` is appended as InfluxDB doesn't support tag names with a leading
    underscore.


# Usage
You can build yourself the image by cloning this repo:
```bash
$ git clone ... && cd dip-to-influx
$ docker build -t dip-to-influx .
$ docker run -v $(pwd)/app:/app -v $(pwd)/subscriptions.txt:/config/subscriptions.txt \
    -e INFLUXDB_USERNAME=username \
    -e INFLUXDB_HOST=localhost \
    -e INFLUXDB_PASSWORD=password \
    -e INFLUXDB_PORT=8086 \
    -e INFLUXDB_DATABASE=data \
    -e CLIENT_NAME=my-dip-client \
    -e POLL_TIME=4 \
    -e SUBSCRIPTIONS_FILEPATH=/config/subscriptions.txt \
    dip-to-influx
```

## Environmental variables
You can set different environmental variables:
* `INFLUXDB_HOST`: the host of the influxdb instance
* `INFLUXDB_USERNAME`: username of the influxdb instance
* `INFLUXDB_PASSWORD`: password of the influxdb instance
* `INFLUXDB_PORT`: port of the influxdb instance
* `INFLUXDB_DATABASE`: name of the database where to write data
* `INFLUXDB_SSL`: wether to enable ssl (default `true`)
* `INFLUXDB_VERIFYSSL`: wether to check ssl certificate (default `false`)
* `SUBSCRIPTIONS_FILEPATH`: the path to a text file where each lines contains a DIP subscription. 
    Must be a valid filepath
* `MEASUREMENT`: the name of the measurement where to write data (default `dip`)
* `DIP_NAMESERVERS`: a string representing DIP nameservers to connect to (default `dipnsgpn1,dipnsgpn2`)
* `LOGGING_LEVEL`: the python logging level (default `INFO`)
* `CLIENT_NAME`: the name of the dip client to register
* `POLL_TIME`: interval between two consecutive insertion to db